import { AfterViewInit, Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { NgLightboxComponent } from './ng-lightbox.component';

@Directive({
  selector : '[siLightboxItem],si-lightbox-item'
})
export class NgLightboxItemDirective implements AfterViewInit {
  @Input() lightboxTitle: string;

  @Input() previewSrc: string;

  index: number;

  src: string;

  current = false;

  protected cssClass = 'si-lightbox-item';

  protected remFn: () => void;

  constructor(protected el: ElementRef, protected rend: Renderer2) {

  }

  @Input() set siLightboxItem(originalSrc: string | null | undefined) {
    this.src = originalSrc ? originalSrc : null;
  }

  ngAfterViewInit(): void {
    if (!this.src) {
      this.src = this.el.nativeElement.dataset && this.el.nativeElement.dataset.src ?
        this.el.nativeElement.dataset.src : (this.el.nativeElement.src || null);
    }

    if (this.src) {
      if (this.el.nativeElement.nodeName !== 'SI-LIGHTBOX-ITEM') {
        this.rend.addClass(this.el.nativeElement, this.cssClass);
        this.rend.setStyle(this.el.nativeElement, 'cursor', 'pointer');
      }

      this.previewSrc = !this.previewSrc ? this.src : this.previewSrc;
    } else {
      this.remFn && this.remFn();
    }
  }

  init(lightbox: NgLightboxComponent, index: number) {
    if (this.remFn) {
      this.remFn();
    }

    this.index = index;
    this.remFn = this.rend.listen(
      this.el.nativeElement,
      'click',
      ($event) => {
        if (!lightbox.isOpened()) {
          this.current = lightbox.open(this);
        }
      }
    );
  }

  isCurrent() {
    return this.current;
  }

  setCurrent(isCurrent: boolean) {
    this.current = isCurrent;
  }

  getSrc() {
    return this.src;
  }
}
