import { NgModule } from '@angular/core';
import { NgLightboxComponent } from './ng-lightbox.component';
import { CommonModule } from '@angular/common';
import { NgLightboxItemDirective } from './ng-lightbox-item.directive';
import { NgLightboxViewComponent } from './ng-lightbox-view.component';
import { EventModifiersModule } from '@silmar/ng-core/event-modifiers';


@NgModule({
  declarations : [ NgLightboxComponent, NgLightboxItemDirective, NgLightboxViewComponent ],
  imports      : [
    CommonModule,
    EventModifiersModule
  ],
  exports      : [ NgLightboxComponent, NgLightboxItemDirective ]
})
export class NgLightboxModule {
}
