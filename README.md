# NG Lightbox

[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-lightbox.svg)](https://www.npmjs.com/package/@silmar/ng-lightbox)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-lightbox/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-ng-lightbox/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-lightbox.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-lightbox)

### Install
```
npm i @silmar/ng-lightbox
// or
yarn add @silmar/ng-lightbox
```

### Demo
[Checkout the demo page](https://etg-public.gitlab.io/silmar-ng-lightbox) or run the demo app, check out the project then run the following in th root directory

```
npm i
ng serve
```

More info in the library [README.md](projects/silmar/ng-lightbox/README.md)

