import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgLightboxModule } from '../../projects/silmar/ng-lightbox/src/lib/ng-lightbox.module';

@NgModule({
  declarations : [
    AppComponent
  ],
  imports      : [
    BrowserModule,
    NgLightboxModule,
    HammerModule
  ],
  providers    : [],
  bootstrap    : [ AppComponent ]
})
export class AppModule {
}
