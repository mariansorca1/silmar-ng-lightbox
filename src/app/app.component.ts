import { Component } from '@angular/core';

@Component({
  selector    : 'app-root',
  templateUrl : './app.component.html',
  styleUrls   : [ './app.component.scss' ]
})
export class AppComponent {
  title           = 'silmar-ng-lightbox';
  gallery: any[]  = [
    [ 'https://images.unsplash.com/photo-1509904785290-42ab09175ef0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&h=600&q=80',
      'https://images.unsplash.com/photo-1509904785290-42ab09175ef0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=150&q=80' ],
    [ 'https://static1.squarespace.com/static/593ac92e6b8f5b0bb5b03648/t/59dae34bedaed846531a2a8f/1507517273573/sugar-glider-1-2.jpg?format=1000w',
      'https://static1.squarespace.com/static/593ac92e6b8f5b0bb5b03648/t/59dae34bedaed846531a2a8f/1507517273573/sugar-glider-1-2.jpg?format=150w' ],
    [ 'https://i.pinimg.com/originals/51/8d/f0/518df057f801845928d688fe9f6e0786.jpg',
      'https://i.pinimg.com/originals/51/8d/f0/518df057f801845928d688fe9f6e0786.jpg' ],
    [ 'https://farm8.staticflickr.com/7066/6891416813_b9244742b1_b.jpg',
      'https://farm8.staticflickr.com/7066/6891416813_b9244742b1_b.jpg' ],
    [ 'https://upload.wikimedia.org/wikipedia/commons/5/5b/Petaurus_Breviceps_Petauro_dello_Zucchero_2.jpg',
      'https://upload.wikimedia.org/wikipedia/commons/5/5b/Petaurus_Breviceps_Petauro_dello_Zucchero_2.jpg' ],
    [ 'https://images.unsplash.com/photo-1536248337121-aebeba487bd4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=80',
      'https://images.unsplash.com/photo-1536248337121-aebeba487bd4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=150&q=80' ],
    [ 'https://upload.wikimedia.org/wikipedia/commons/9/97/Sugar_Gliders_eating_Mealworms.jpg',
      'https://upload.wikimedia.org/wikipedia/commons/9/97/Sugar_Gliders_eating_Mealworms.jpg' ],
    [ 'https://cdn.britannica.com/96/180396-138-CA8FCDFD/chipmunks-Siberian-seeds.jpg?w=1000',
      'https://cdn.britannica.com/96/180396-138-CA8FCDFD/chipmunks-Siberian-seeds.jpg?w=150' ],
    [ 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/eastern-chipmunk-tamius-striatus-michigan-usa-royalty-free-image-1595624602.jpg',
      'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/eastern-chipmunk-tamius-striatus-michigan-usa-royalty-free-image-1595624602.jpg' ],
    [ 'https://khabaradda.online/wp-content/uploads/2021/08/watch-chipmunk-binges-on-plateful-of-hazelnuts-so-adorable-twitter-reacts.jpg',
      'https://khabaradda.online/wp-content/uploads/2021/08/watch-chipmunk-binges-on-plateful-of-hazelnuts-so-adorable-twitter-reacts.jpg' ],
    [ 'https://cdn1.tedsby.com/tb/large/storage/5/4/9/549146/stuffed-other-sugar-glider-by-yulia-leonovich.jpg',
      'https://cdn1.tedsby.com/tb/large/storage/5/4/9/549146/stuffed-other-sugar-glider-by-yulia-leonovich.jpg' ],
    [ 'https://cdn1.tedsby.com/tb/large/storage/2/9/6/296573/artist-toy-mouse-baby-sugar-glider.jpg',
      'https://cdn1.tedsby.com/tb/large/storage/2/9/6/296573/artist-toy-mouse-baby-sugar-glider.jpg' ],
    [ 'https://www.thesprucepets.com/thmb/TSwWWSVEr7czKdw_iRQAyrof5lo=/735x0/96747320-crop-56a2bcc43df78cf772795fbf.jpg',
      'https://www.thesprucepets.com/thmb/TSwWWSVEr7czKdw_iRQAyrof5lo=/735x0/96747320-crop-56a2bcc43df78cf772795fbf.jpg' ],
    [ 'https://www.thesprucepets.com/thmb/o6IoYWzuwB6Ojup_mGx42ZYhIQg=/1414x1414/smart/filters:no_upscale()/sugarglider-GettyImages-167156810-5c36b9acc9e77c0001e4e279.jpg',
      'https://www.thesprucepets.com/thmb/o6IoYWzuwB6Ojup_mGx42ZYhIQg=/1414x1414/smart/filters:no_upscale()/sugarglider-GettyImages-167156810-5c36b9acc9e77c0001e4e279.jpg' ],
    [ 'https://www.boobookecotours.com.au/wp-content/uploads/Untitled-design-2.jpg',
      'https://www.boobookecotours.com.au/wp-content/uploads/Untitled-design-2.jpg' ],
    [ 'https://www.thesprucepets.com/thmb/DSmhK3SVI9q_fQn5pIxgq1fZ8Hg=/4377x3283/smart/filters:no_upscale()/high-angle-view-of-sugar-glider-relaxing-on-rug-1028350916-5c4f122bc9e77c0001d7619b.jpg',
      'https://www.thesprucepets.com/thmb/DSmhK3SVI9q_fQn5pIxgq1fZ8Hg=/4377x3283/smart/filters:no_upscale()/high-angle-view-of-sugar-glider-relaxing-on-rug-1028350916-5c4f122bc9e77c0001d7619b.jpg' ],
    [ 'https://a-z-animals.com/media/2022/01/shutterstock_1120547291-1024x535.jpg',
      'https://a-z-animals.com/media/2022/01/shutterstock_1120547291-1024x535.jpg' ],
    [ 'https://cdn.britannica.com/72/227472-050-B054D646/Malayan-colugo-Galeopterus-variegatus.jpg',
      'https://cdn.britannica.com/72/227472-050-B054D646/Malayan-colugo-Galeopterus-variegatus.jpg' ],
    [ 'https://i.pinimg.com/originals/28/14/21/281421bd8fd891ca3bdeb92234f61de3.jpg',
      'https://i.pinimg.com/originals/28/14/21/281421bd8fd891ca3bdeb92234f61de3.jpg' ]
  ];
  gallery2: any[] = [
    [ 'https://images.unsplash.com/photo-1509904785290-42ab09175ef0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=80',
      'https://images.unsplash.com/photo-1509904785290-42ab09175ef0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=150&q=80' ],
    [ 'https://static1.squarespace.com/static/593ac92e6b8f5b0bb5b03648/t/59dae34bedaed846531a2a8f/1507517273573/sugar-glider-1-2.jpg?format=1000w',
      'https://static1.squarespace.com/static/593ac92e6b8f5b0bb5b03648/t/59dae34bedaed846531a2a8f/1507517273573/sugar-glider-1-2.jpg?format=150w' ],
    [ 'https://images.unsplash.com/photo-1536248337121-aebeba487bd4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=80',
      'https://images.unsplash.com/photo-1536248337121-aebeba487bd4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=150&q=80' ],
    [ 'https://cdn.britannica.com/96/180396-138-CA8FCDFD/chipmunks-Siberian-seeds.jpg?w=1000',
      'https://cdn.britannica.com/96/180396-138-CA8FCDFD/chipmunks-Siberian-seeds.jpg?w=150' ],
  ];

  txt = {
    install : 'yarn add @silmar/ng-lightbox hammerjs',
    module  : `import {NgLightboxModule} from '@silmar/ng-lightbox';
import { HammerModule } from '@angular/platform-browser';
// ...
@NgModule({
    declarations: [ AppComponent ],
    imports: [
        //...
        HammerModule, // <-- For Angular 9
        NgLightboxModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,
    ex1     : `<si-ng-lightbox>
  <img *ngFor="let img of gallery2" src="{{img[0]}}" height="200" [siLightboxItem]="img[1]" class="item"/>
</si-ng-lightbox>`,
    ex2     : `<si-ng-lightbox [showPreviews]="true">
  <img *ngFor="let img of gallery;let i = index" src="{{img[0]}}" height="200" [previewSrc]="img[1]" [siLightboxItem]="img[1]" class="item"
      [lightboxTitle]="'Test title for caption #' + i" />
</si-ng-lightbox>`,
    ex3     : `<si-ng-lightbox #light>
  <button (click)="light.open()">Open Lightbox</button>
  <si-lightbox-item *ngFor="let img of gallery2" [siLightboxItem]="img[1]"></si-lightbox-item>
</si-ng-lightbox>`,
  };

}
